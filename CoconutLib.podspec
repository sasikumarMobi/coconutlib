#
# Be sure to run `pod lib lint Umbrella.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#
#
Pod::Spec.new do |s|
s.name              = 'CoconutLib'
s.version           = '1.0.0'
s.summary           = 'Description of MOBIPlayer Release Framework.'
s.description      = <<-DESC
A bigger description of MOBIPlayer Release Framework.
DESC
s.homepage          = 'https://mobiotics.com'
s.license           = { :type => 'MIT', :text => 'The MIT license (MIT) \n Copyright (c) Sasikumar <sasikumar.d@mobiotics.com>.' }
s.authors           = { 'Sasikumar' => 'sasikumar.d@mobiotics.com'}
s.source            = { :git => 'https://sasikumarMobi@bitbucket.org/sasikumarMobi/mobiplayerreleaseframework.git', :tag => s.version
}
s.ios.vendored_frameworks = 'CoconutLib.xcframework'
s.ios.deployment_target = '10.3'
# Add all the dependencies
end
